# README #

EpiTracker is used to track your favorite TV Shows so you never miss anything. It uses [thetvdb](http://thetvdb.com/) open database.

### How do I get set up? ###

* You need to have the rails environment set up
* Run `bundle install` followed by `rake db:create` and `rake db:migrate`
* Run `bower install`
* In order to populate the database edit the ` app\series_names.txt` file to contain a list of TV Shows. After that run: `rake get_series:get_list_series`
and `rake get_series:get_episodes`
* To keep the db updated run `rake get_series:update`
