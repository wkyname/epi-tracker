#= require_tree ./templates
#= require_self
#= require_tree ./services
#= require_tree ./controllers
#= require_tree ./directives
#= require_tree ./filters

angular.module 'EpiTrackerApp', [
  'ngRoute',
  'ngAnimate',
  'templates',
  'ng-token-auth'
]


angular.module 'EpiTrackerApp'
  .config [ '$authProvider', ($authProvider) ->
    $authProvider.configure {
      apiUrl:                  '/api'
    }
  ]
  .config [ '$routeProvider', '$locationProvider', 'routeHelper',
    ($routeProvider, $locationProvider, routeHelper) ->
      authResolver = {
        auth: [ "$auth", ($auth) ->
              $auth.validateUser()
        ]
      }
      $locationProvider.html5Mode(true)
      $routeProvider.when(routeHelper.series_path()
        {
          templateUrl: '_series.html'
          controller: 'SeriesController'
        }
      ).when(routeHelper.series_all_path()
        {
          templateUrl: '_series_all.html'
          controller:  'SeriesAllController'
        }
      ).when(routeHelper.welcome_path()
        {
          templateUrl: '_welcome.html'
          controller: 'welcomeController'
        }
      ).when(routeHelper.dashboard_path()
        {
          templateUrl: '_dashboard.html'
          controller: 'DashboardController'
          resolve: authResolver
        }
      ).when(routeHelper.signup_path()
        {
          templateUrl: '_signup.html'
          controller: 'SignupController'
        }
      ).when(routeHelper.profile_path()
        {
          templateUrl: 'users/_index.html'
          controller: 'OwnProfileController'
          resolve: authResolver
        }
      ).when(routeHelper.edit_profile_path()
        {
          templateUrl: 'users/_edit.html'
          controller: 'OwnProfileController'
          resolve: authResolver
        }
      ).when(routeHelper.users_path()
        {
          templateUrl: 'users/_show.html'
          controller: 'UserProfileController'
          resolve: angular.extend({
            user_page: ['$route', 'userService', ($route, userService) ->
              return userService.get($route.current.params.nickname)
            ]
          }, authResolver)
        }
      ).when(routeHelper.unwatched_path()
        {
          templateUrl: '_unwatched.html'
          controller: 'UnwatchedController'
          resolve: authResolver
        }
      ).otherwise(
        {
          templateUrl: '_welcome.html'
          controller: 'welcomeController'
        }
      )
  ]
  .config [
    '$httpProvider', ($httpProvider)->
      $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content')
  ]
  .run [
    '$route', '$rootElement', ($route, $rootElement) ->
      # $rootElement.off('click')
  ]
  .run [
    '$rootScope', '$location', "routeHelper", ($rootScope, $location, routeHelper) ->
      $rootScope.$on '$routeChangeError', (event, current, previous, error) ->
        if (error.status == 404)
          $location.path(routeHelper.not_found_path()).replace()
        if (error.reason == "unauthorized")
          $location.path(routeHelper.signup_path()).replace()
          $rootScope.$broadcast "alert:danger:new", "Please log in before continuing"
      # $rootScope.$on '$routeChangeSuccess', ($event, current) ->
      #   debugger
  ]
