angular.module "EpiTrackerApp"
  .controller "DashboardController", ["$scope", "$http","$compile", "$animate", "seriesProv", ($scope, $http, $compile, $animate, seriesProv)->
    $scope.activeSeries = []
    $scope.inactiveSeries = []
    seriesProv.getOwnSeries(true).then(
      (data)->
        i=0
        while i <data.series.length
          serie = data.series[i]
          if(serie.newDate != "TBD")
            $scope.activeSeries.push(serie)
          else
            $scope.inactiveSeries.push(serie)
          i++

    ).catch()

    $scope.section =
      "upcoming": true,
      "all": false,
      "friends": false

    $scope.changeSection = (sec) ->
      if($scope.section[sec] == true)
        return
      else
        $scope.section.upcoming = false
        $scope.section.all = false
        $scope.section.friends = false
        $scope.section[sec] = true
  ]
