angular.module "EpiTrackerApp"
  .controller "MainController", [ "$scope", "$document", "$window", "$location", "watchProv", "authService", "routeService",
    ($scope, $document, $window, $location, watchProv, authService, routeService) ->
      $scope.Routes= () -> Routes
      $scope.AppRoutes= routeService.navRoutes()
      $scope.goTo = (address, e) ->
        $location.path address

      # blabla = routeService.navRoutes()
      $scope.user
      $scope.$on "$viewContentLoaded", () ->
        $.material.init()
        # $anchorScroll()

      $scope.$on "navigate:to", (event, target) ->
        $scope.goTo target

      $scope.$on "auth:validation-success", (event, user) ->
        $scope.user = user
        authService.setUserLoggedIn true
        $scope.$emit "alert:success:new", "Welcome back #{user.nickname}"

      $scope.$on "auth:validation-error", (event, error) ->
        console.log("validation-error")

      $scope.$on "auth:invalid", (event, error) ->
        console.log("auth:invalid");

      $scope.$on "auth:session-expired", (event, error) ->
        console.log("session-expired")
        $scope.$emit "alert:danger:new", "Session Expired! Please log in again"

      $scope.$on "auth:login-success", (event, user) ->
        $scope.user = user
        authService.setUserLoggedIn true
        $location.path($scope.AppRoutes.dashboard_path()).replace()
        $scope.$emit "alert:success:new", "Succesfully logged in"

      $scope.$on "auth:logout-success", (event) ->
        $scope.user = {}
        authService.setUserLoggedIn false
        $location.path($scope.AppRoutes.welcome_path()).replace()
        $scope.$emit "alert:success:new", "Goodbye!"

      $scope.$on "auth:signup-success", (event) ->
        authService.validateUser()
        $location.path($scope.AppRoutes.welcome_path()).replace()

      $scope.$on "auth:registration-email-error", (event, reason) ->
        $scope.$emit "alert:danger:new", reason.errors.full_messages[0]


      $scope.user_signed_in= () ->
        authService.isUserLoggedIn()

      $scope.logout= () ->
        authService.logout()

      $scope.addSeries = (show) ->
        watchProv.addEntry(show.id).then(
          (data)->
            if(typeof data.followers != 'undefined')
              show.followers = data.followers
            if(typeof data.following != 'undefined')
              show.following = data.following
        ).catch(
          (res, error) ->
            console.log("Handle Error")
        )



  ]
