angular.module "EpiTrackerApp"
  .controller "OwnProfileController", [
    "$scope", "authService", "seriesProv", ($scope, authService, seriesProv)->
      # $scope.dataset = []

      # $scope.dataset = [
      #   { label: 'Abulia', count: 10 },
      #   { label: 'Betelgeuse', count: 20 },
      #   { label: 'Cantaloupe', count: 30 },
      #   { label: 'Dijkstra', count: 40 }
      # ]


      $scope.edit_user = angular.copy($scope.user)
      # angular.copy($scope.user, $scope.edit_user)
      $scope.edit_profile= () ->
        $scope.goTo('/profile/edit')
      $scope.backButton= () ->
        window.history.back()
      $scope.save= (form) ->
        if (form.$valid)
          authService.update_user $scope.edit_user, $scope.successCallback
      $scope.successCallback= () ->
        $scope.goTo('/profile')
  ]
  .controller "UserProfileController", ["$scope", "user_page", ($scope, user_page) ->
    $scope.user_profile = user_page
  ]
