angular.module "EpiTrackerApp"
  .controller "SeriesAllController", ["$scope", "seriesProv", ($scope, seriesProv) ->
    seriesProv.getAll(1).then(
      (data)->
        $scope.series = data.series
    )
  ]
