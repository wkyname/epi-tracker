angular.module "EpiTrackerApp"
  .controller "SeriesController", ["$scope", "$routeParams", "seriesProv", ($scope, $routeParams, seriesProv) ->

    seriesProv.getSeriesById($routeParams.id).then(
      (data)->
        # assign info
        $scope.currentSeries = data.serie
        $scope.currentSeries.followers = data.followers
        $scope.currentSeries.following = data.following
        $scope.toggleDesc()
        seriesProv.getSeasons($routeParams.id).then(
          (data)->
            $scope.currentSeries.seasons = data.seasons
            $scope.currentSeason = 0
            seriesProv.getSeasonEpisodes($scope.currentSeries.seasons[0].id).then(
              (epData) ->
                $scope.currentSeries.episodes = epData.episodes
            ).catch( (res, error) ->
              console.log("Handle Error")
            )
        ).catch(
          (res, err) ->
            console.log("Handle Error")
        )
    ).catch( (res, error) ->
      console.log("Handle Error")
    )

    $scope.unfollow = (serie)->
      seriesProv.unfollowSerie(serie.id).then(
        (data)->
          if(typeof data.followers != 'undefined')
            serie.followers = data.followers
          if(typeof data.following != 'undefined')
            serie.following = data.following
      )

    $scope.activeDesc =
      active: true,
      text : ""



    $scope.selectSeason = (nr) ->
      if($scope.currentSeason != nr)
        seriesProv.getSeasonEpisodes($scope.currentSeries.seasons[nr].id)
        .then(
          (data)->
            $scope.currentSeries.episodes = data.episodes
        ).catch(
          (res, abc) ->
            console.log("Handle Error")
        )
        $scope.currentSeason = nr

    $scope.isActive = (season_nr) ->
      $scope.currentSeason == season_nr

    $scope.watchSeason = ()->
      seriesProv.tickSeason($scope.currentSeries.seasons[$scope.currentSeason].id)
      i = 0
      while i < $scope.currentSeries.episodes.length
        if($scope.currentSeries.episodes[i].watchable)
          $scope.currentSeries.episodes[i].seen = true
        i++


    $scope.toggleDesc = ->
      if($scope.activeDesc.active)
        $scope.activeDesc.text = $scope.currentSeries.overview.substring(0, 240) + "..."
      else
        $scope.activeDesc.text = $scope.currentSeries.overview
      $scope.activeDesc.active = !$scope.activeDesc.active





  ]
