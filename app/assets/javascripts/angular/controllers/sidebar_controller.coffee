angular.module "EpiTrackerApp"
  .controller "sidebarController", ["$scope", "$window", "authService" ,($scope, $window, authService) ->

    container = document.querySelector("#st-container")
    button    = document.querySelector("#sidebar")

    hasParentClass =  (e, classname) ->
      if(e == document)
        return false
      if(e.classList.contains classname )
        return true
      return hasParentClass e.parentNode, classname

    bodyClickFn = (evt)->
      if !hasParentClass evt.target, 'st-menu'
        resetMenu()
        document.removeEventListener "click", bodyClickFn

    resetMenu = ->
      container.classList.remove "st-menu-open"

    triggerSideBar = ->
      if $window.innerWidth < 992
        container.classList.remove 'st-menu-open'
      else
        container.classList.add("st-menu-open")
        document.removeEventListener "click", bodyClickFn

    # on resize trigger bar
    $window.addEventListener "resize", triggerSideBar

    triggerSideBar()

    # add menu trigger on click
    button.addEventListener 'click', (ev) ->
      ev.stopPropagation()
      ev.preventDefault()
      container.className = "st-menu-open"
      document.addEventListener 'click', bodyClickFn
  ]
