angular.module "EpiTrackerApp"
.controller "SignupController", ["$scope", "usersProv", "authService",  ($scope, usersProv, authService)->
    $scope.newUser = {}
    $scope.tempUser ={}
    $scope.login_user = {
      nickname: ''
      password: ''
    }

    $scope.create_user = {
      nickname: ''
      email: ''
      password: ''
      password_confirmation: ''
    }

    $scope.signup = (form) ->
      if (form.$valid)
        authService.signup($scope.create_user)

    $scope.login = (form) ->
      if (form.$valid)
        authService.login $scope.login_user
    $scope.signUp = (form) ->
      if(form.$valid)
        $scope.update($scope.tempUser)
        authService.signup(form)

    $scope.update = (tempUser) ->
      $scope.newUser = angular.copy(tempUser)

    $scope.signIn = (form)->
      if(form.$valid)
        usersProv.checkUser($scope.login).then(
          (data)->
            # console.log(data.user.length)
        )
  ]
