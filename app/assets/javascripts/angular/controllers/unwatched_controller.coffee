angular.module "EpiTrackerApp"
  .controller "UnwatchedController", ["$scope", "$http", "$compile", "$animate", "$window", "$document", "$timeout", "seriesProv", "unwatchedFilter", ($scope, $http, $document, $compile, $animate, $window,  $timeout, seriesProv, unwatchedFilter)->
    $scope.currentSeries = {}
    $scope.currentSeasons = []

    $scope.activeSeason = null
    $scope.activeSerie  = null
    $scope.displayedSeason = null



    seriesProv.getOwnSeries(false).then(
      (data)->
        $scope.series = data.series
    )

    $scope.$watch('activeSeason',
      ()->
        if($scope.activeSeason != null && typeof $scope.activeSeason != 'undefined')
          $scope.displayedSeason = $scope.currentSeasons[$scope.activeSeason]
    )


    $scope.$watch('displayedSeason.has_episodes',
      ()->
        if($scope.displayedSeason != null && typeof $scope.displayedSeason != 'undefined')
          if(!$scope.displayedSeason.has_episodes)
            if($scope.currentSeasons.length > $scope.activeSeason + 1 )
              $scope.selectSeason($scope.activeSeason+1)
    )

    $scope.toggleSeries = (serie)->
      $scope.currentSeries.active = false
      serie.active = true
      $scope.currentSeries = serie

      seriesProv.getSeasons(serie.id, true).then(
        (seasons)->
          $scope.currentSeasons = seasons.seasons
          $scope.activeSeason = null
          $scope.selectSeason(0)
          $scope.slide()
      )

    $scope.slide = ()->
      angular.element("html, body").animate {scrollTop: angular.element('#seasons').offset().top - 100}, 800

    $scope.selectSeason = (nr) ->
      if($scope.activeSeason!=null)
        $scope.currentSeasons[$scope.activeSeason].isActive = false
      if($scope.activeSeason != nr)
        $scope.activeSeason = nr
        seriesProv.getSeasonEpisodes($scope.currentSeasons[nr].id, true)
        .then(
          (data)->
            if(data.episodes.length > 0)
              $scope.currentSeries.episodes = data.episodes
            else
              $scope.currentSeasons[nr].has_episodes = false
              if($scope.currentSeasons.length > nr + 1 )
                $scope.selectSeason(nr+1)
        )
      $scope.currentSeasons[nr].isActive = true


    $scope.watchSeason = ( nr )->
      seriesProv.tickSeason($scope.currentSeasons[nr].id).then(
        (response)->
          if response.success
            i = 0
            while i < $scope.currentSeries.episodes.length
              if(!$scope.currentSeries.episodes[i].seen)
                $scope.currentSeries.unseen_episodes--
              $scope.currentSeries.episodes[i].seen = true
              i++
            $timeout(
              ()->
                $scope.currentSeasons[nr].has_episodes = false
                if nr+1 < $scope.currentSeasons.length
                  $scope.selectSeason(nr+1)
                else
                  $scope.currentSeries.active = false
              , 500
            )
      )
  ]
