angular.module "EpiTrackerApp"
  .directive "alertbox", [ ()->
    restrict: 'E',
    templateUrl: '_alert.html'
    controller: ["$scope", "$rootScope", "$timeout", ($scope, $rootScope, $timeout)->
      $scope.timeoutPromise
      $scope.alertShown = false
      $scope.alertType = "alert-success"
      $scope.alertMsg = "You've succesfully logged in"
      $rootScope.$on "alert:danger:new", (event, alert) ->
        $timeout.cancel($scope.timeoutPromise)
        $scope.alertType = "alert-danger"
        $scope.alertMsg = alert
        $scope.alertShown = true
        $scope.timeoutPromise = $timeout($scope.closeAlert, 2000)
        return

      $rootScope.$on "alert:success:new", (event, alert) ->
        $timeout.cancel($scope.timeoutPromise)
        $scope.alertType = "alert-success"
        $scope.alertMsg = alert
        $scope.alertShown = true
        $scope.timeoutPromise = $timeout($scope.closeAlert, 2000)
        return

      $scope.closeAlert = () ->
        $timeout.cancel($scope.timeoutPromise)
        $scope.alertShown = false
    ]
  ]
