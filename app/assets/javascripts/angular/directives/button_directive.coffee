angular.module "EpiTrackerApp"
  .directive "btn", ["$timeout", ($timeout)->
    restrict: 'C'
    link: (scope, element, attrs) ->
      $(element).ripples()
      return
  ]
  .directive "checkbox", ["$timeout", ($timeout)->
    restrict: 'C'
    link: (scope, element, attrs) ->
      exec_checkbox = () ->
        $.material.checkbox()
      $timeout exec_checkbox, 10
  ]

