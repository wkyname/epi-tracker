angular.module "EpiTrackerApp"
  .directive "chart", [ ()->
    restrict: 'E'
    scope:
      mydata: '='
    templateUrl:'_chart.html'
    link: (scope, element, attrs)->


    controller:["$scope", "seriesProv", "$window", "$element",($scope, seriesProv, $window, $element)->
      w = angular.element($window)
      # debugger
      $scope.element = $element[0]
      w.bind('resize', ()->
          $scope.changeWidth($element.width())
        )
      $scope.width = $element.width()

      # get data
      seriesProv.getOwnSeries(true).then(
        (data) ->
          $scope.series = data.series
          $scope.dataset = []
          total = 0
          i =0
          $scope.series.forEach((serie)->
            gen = serie.genre
            total = total + gen.length
            gen.forEach((g)->
              $scope.addGenre(g)
              )
            )
          if($scope.dataset.length > 7)
            $scope.filterData($scope.dataset, total)
          $scope.generateChart($scope.dataset)
      )

      $scope.filterData = (data, totalData)->
        other = {label: 'other', count:0}
        treshold = 1/10
        while(data.length > 7)
          data.forEach( (g) ->
            if(g.count < totalData * treshold/10)
              other.count = other.count + g.count
              data.splice(data.indexOf(g), 1)
            )
          treshold = treshold + 0.5/10
        data[data.length] = other

      $scope.addGenre = (type) ->
        i = 0
        check = false
        while i < $scope.dataset.length && !check
          if($scope.dataset[i].label == type)
            $scope.dataset[i].count++
            check = true
          i++
        if (!check)
          $scope.dataset[$scope.dataset.length]= {label: type, count: 1}

      $scope.changeWidth = (width)->
        donutWidth = width/5
        radius = width/2
        d3.select('#'+$scope.element.children[0].id).select('svg').attr('width', radius*2)
              .attr('height', radius*2)
              .select('g')
              .attr('transform', 'translate(' + radius+  ',' + radius + ')')
        $scope.arc.outerRadius(radius).innerRadius(radius - donutWidth)
        $scope.path.attr('d', $scope.arc)


      $scope.generateChart = (mydata)->
        # debugger
        legendRectSize = 15
        legendSpacing = 3
        radius = $scope.width / 2
        donutWidth = $scope.width/5
        color = d3.scale.category20b()
        # debugger
        $scope.svg = svg = d3.select('#'+$scope.element.children[0].id)
                .append('svg')
                .attr('width', $scope.width)
                .attr('height', $scope.width)
                .append('g')
                .attr('transform', 'translate(' + ($scope.width / 2) +  ',' + ($scope.width / 2) + ')')
        $scope.arc = d3.svg.arc()
                    .outerRadius(radius).innerRadius(radius- donutWidth)
        pie = d3.layout.pie()
                      .value((d)->  return d.count )
                      .sort(null)
        $scope.path = svg.selectAll('path')
                  .data(pie(mydata))
                  .enter()
                  .append('path')
                  .attr('d', $scope.arc)
                  .attr('fill', (d, i)->
                    return color(d.data.label);
                  )
        legend = svg.selectAll('.legend')
                  .data(color.domain())
                  .enter()
                  .append('g')
                  .attr('class', 'legend')
                  .attr('transform', (d, i)->
                    height = legendRectSize + legendSpacing;
                    offset = height * color.domain().length / 2;
                    horz = -2 * legendRectSize;
                    vert = i * height - offset;
                    return 'translate(' + horz + ',' + vert + ')';
                  )
        legend.append('rect')
              .attr('width', legendRectSize)
              .attr('height', legendRectSize)
              .style('fill', color)
              .style('stroke', color);
        legend.append('text')
              .attr('x', legendRectSize + legendSpacing)
              .attr('y', legendRectSize - legendSpacing)
              .text((d)->
                return d );

    ]
  ]
