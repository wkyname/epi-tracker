angular.module "EpiTrackerApp"
  .directive "episodestable", [ "seriesProv", (seriesProv)->
    restrict: 'E',
    scope:
      episodes: '='
      removeepisodes: '='
      season: '='
      serie: '='
    templateUrl: '_episodesTable.html'
    controller: ["$scope", ($scope)->
      $scope.is_episode_expanded = false
      $scope.expanded_episode= null

      $scope.$watch('episodes',
        ()->
          if($scope.episodes != undefined && $scope.removeepisodes)
            $scope.unseen = $scope.episodes.length
            if( $scope.unseen == 0 )
              $scope.season.has_episodes = false
      )

      $scope.$watch('unseen',
        ()->
          if($scope.removeepisodes)
            if($scope.unseen == 0)
              $scope.season.has_episodes = false
      )

      $scope.descriptionOn = (episode)->
        if($scope.removeepisodes && episode.seen)
          return false
        if $scope.isExpanded(episode)
          return true

      $scope.showDescription = (episode) ->
        if ($scope.expanded_episode != episode)
          $scope.expanded_episode = episode
          $scope.is_episode_expanded = true
        else
          $scope.expanded_episode = null
          $scope.is_episode_expanded = false
        return

      $scope.checkEpisode = (episode)->
        remove = @removeepisodes
        show = @serie
        if(!episode.seen)
          seriesProv.untickEpisode(episode.id).then(
            (response)->
              if response.success
                if remove
                  show.unseen_episodes++
                  $scope.unseen++
          )
        else
          seriesProv.tickEpisode(episode.id).then(
            (response)->
              if response.success
                if remove
                  show.unseen_episodes--
                  $scope.unseen--
          ).catch( (res, err) ->
            episode.seen = false
            $scope.$emit "alert:danger:new", res.errors[0]
          )

      $scope.isExpanded= (episode) ->
        $scope.is_episode_expanded && ($scope.expanded_episode == episode)
    ]
  ]
