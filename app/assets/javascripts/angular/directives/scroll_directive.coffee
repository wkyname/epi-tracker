angular.module "EpiTrackerApp"
  .directive 'scrollOnClick', ->
    restrict: 'A'
    link: (scope, $elm, attrs) ->
      idToScroll = attrs.href
      $elm.on 'click', ->
        $target = undefined
        if idToScroll
          $target = $(idToScroll)
        else
          $target = $elm
        $('body').animate { scrollTop: $target.offset().top }, 'slow'
        return
      return
