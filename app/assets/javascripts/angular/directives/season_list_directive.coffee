angular.module "EpiTrackerApp"
  .directive "seasonlist", [ "seriesProv", (seriesProv)->
    restrict: 'E',
    scope:
      seasons: '='
    templateUrl: '_season_list.html'
    controller: ["$scope", ($scope)->
    ]
  ]
