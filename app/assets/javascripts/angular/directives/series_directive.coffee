angular.module "EpiTrackerApp"
  .directive "showthumb", [ () ->
    restrict: 'E'
    bindToController: true
    scope:
      serie: '='
      # name: '='
      # followers: '='
      # following: '='
      # cover: '='
      # imdb: '='
      # date: '='
      goto: '='
      # id:   '='
      followstatus: '='
      episodes: '='
      # active: '='
      gotolink: '='
    templateUrl: '_showThumb.html'
    link : (scope, element, attrs )->
      cover = $(element).find('.cover.loading')
      img = $(cover).find('img')
      $(img).load(
        ()->
          $(cover).removeClass('.loading')
          $(this).fadeIn()
      )
    controllerAs: 'st'
    controller: [ "seriesProv", "$scope", (seriesProv, $scope) ->

      @unfollowSerie = (serie) =>
        seriesProv.unfollowSerie(serie.id).then(
          (data)->
            if(typeof data.followers != 'undefined')
              serie.followers = data.followers
            if(typeof data.following != 'undefined')
              serie.following = data.following
        )

      @shortName = (name) =>
        if(name.length > 20)
          shortName = name.substring(0, 17)+'...'
          return shortName
        return name

      @addSeries= (serie) =>
        seriesProv.addSeries(serie.id).then(
          (data)->
            if(typeof data.followers != 'undefined')
              serie.followers = data.followers
            if(typeof data.following != 'undefined')
              serie.following = data.following
        ).catch( (error) ->
          $scope.$emit "alert:danger:new", "You need to be logged in to follow a serie"
        )

      @goTo= (address, e) =>
        if (@gotolink)
          if (angular.element("#insertPicture").size() != 0) then angular.element("#insertPicture").remove()
          templ = "<div id='insertPicture' class='container-fluid'><div class='row'><div class='col-xs-12'><div class='cover-container'><div class='cover'><img class='full' src='"+e.target.getAttribute("ng_src")+"'/></div></div></div></div></div>"
          # templ = '<p>test</p>'
          target = angular.element(e.target).clone()
          view = angular.element("[ng_view]")
          overlay = view.append(templ)
          # debugger
          overlay.find("#insertPicture").css
            "position": "absolute"
            "top": (e.pageY - e.offsetY - view.offset().top - angular.element(window).scrollTop())
            "left": (e.pageX - e.offsetX )
            "width": e.target.width
            "z-index": -1
          # debugger
          overlay.find("#insertPicture").addClass("beforeTransition")
          if ( angular.element(window).scrollTop() != 0 )
            angular.element("html, body").animate {scrollTop: 0}, 800
          @goto(address, e)
          return
      return
    ]
  ]
