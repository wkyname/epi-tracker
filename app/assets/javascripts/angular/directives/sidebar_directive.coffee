angular.module "EpiTrackerApp"
	.directive 'sidebar', [()->
		restrict: 'E'
		controller: 'sidebarController'
		templateUrl: '_sidebar.html'
	]