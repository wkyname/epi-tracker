angular.module "EpiTrackerApp"
  .filter "unwatched", [ ()->
    return (input) ->
      if (input == undefined) 
        return false
      # console.log(input)
      i = 0
      while i<input.length
        if input[i].unseen_episodes == 0
          input.splice(i,1)
        i++
      return input
  ]
