epitracker = angular.module 'EpiTrackerApp'
epitracker.factory 'apiRequest', [
  '$http', '$q', ($http, $q)->
    return {
      get: (path)->
        defferedPromise = $q.defer()
        $http.get(path).success( (response)->
          defferedPromise.resolve(response)
        ).error (data, status, headers, config) ->
          error = {
            data: data
            status: status
          }
          defferedPromise.reject(error)
        defferedPromise.promise
      post: (path, object) ->
        defferedPromise = $q.defer()
        $http.post(path, object).success( (response) ->
          defferedPromise.resolve(response)
        ).error (response) ->
          defferedPromise.reject(response)
        defferedPromise.promise
      delete:(path, object) ->
        defferedPromise = $q.defer()
        $http.delete(path, object).success((response)->
          defferedPromise.resolve(response)
        ).error (response) ->
          defferedPromise.reject(response)
        defferedPromise.promise
      put:(path, object) ->
        defferedPromise = $q.defer()
        $http.put(path, object).success((response) ->
          defferedPromise.resolve(response)
        ).error (response) ->
          defferedPromise.reject(response)
        defferedPromise.promise

    }
]
epitracker.factory 'authService', [
  '$auth', '$q', '$rootScope', ($auth, $q, $rootScope) ->
    loggedIn = false
    return {
      signup: (data) ->
        $auth.submitRegistration(data)
          .then (res) ->
            $rootScope.$broadcast "auth:signup-success"
            console.log("Success registration")
          .catch (res) ->
            console.log("Error error signup #panic")
      login: (data) ->
        $auth.submitLogin(data)
          .then (res) ->
            loggedIn = true
          .catch (res) ->
            console.log("Invalid login")
      logout: ->
        $auth.signOut()
          .then (res) ->
            loggedIn = false
      update_user: (data, successCallback) ->
        $auth.updateAccount(data)
          .then (res) ->
            successCallback()
          .catch (res) ->
            console.log("Handle Error")
      isUserLoggedIn: () ->
        loggedIn
      setUserLoggedIn: (logValue) ->
        loggedIn = logValue
      validateUser: () ->
        $auth.validateUser()
    }
]

epitracker.factory 'userService', [
  'apiRequest', 'routeService', (apiRequest, routeService) ->
    return {
      get: (nickname) ->
        return apiRequest.get(routeService.apiRoutes().api_users_path({format: "json", nickname: nickname}))
    }
]

epitracker.factory 'routeService', [
  "routeHelper", (routeHelper) ->
    rs = Routes
    nr = routeHelper
    return {
      navRoutes: () ->
        return nr
      apiRoutes: () ->
        return rs
    }
]
