epitracker = angular.module 'EpiTrackerApp'
epitracker.constant 'routeHelper', {
    root_path:      () -> return "/"
    welcome_path:   () -> return "/welcome"
    dashboard_path: () -> return "/dashboard"
    unwatched_path: () -> return "/unwatched"
    signup_path:    () -> return "/signup"
    profile_path:   () -> return "/profile"
    edit_profile_path:  () -> return "/profile/edit"
    users_path: (nickname = ":nickname") ->
      return "/users/" + nickname
    series_path: (id = ":id") ->
      return "/series/" + id
    series_all_path: () ->  return "/series_all"
    default_path: () -> return "/"
    not_found_path: () -> return "/404"
  }
