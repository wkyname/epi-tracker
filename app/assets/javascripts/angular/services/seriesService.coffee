angular.module "EpiTrackerApp"
  .service "seriesProv", ["$http", "apiRequest", "routeService", ($http, apiRequest, routeService)->
    apiRoutes = routeService.apiRoutes()
    return {
      addSeries : (show_id)->
        apiRequest.post(apiRoutes.api_watchlists_path({format: "json"}), {serie_id: show_id})

      unfollowSerie: (serie_id)->
        apiRequest.delete(apiRoutes.api_watchlist_path(serie_id, {format: "json"}))

      getSeriesById : (id) ->
        apiRequest.get(apiRoutes.api_series_path(id, {format: "json"}))

      getSeasonEpisodes : (season_id, seen) ->
        if(seen != undefined)
          apiRequest.get(apiRoutes.api_season_user_episodes_path(season_id ,{format: "json"}))
        else
          apiRequest.get(apiRoutes.api_season_episodes_path(season_id, {format: "json"}))
          
      getSeasons : (series_id, w) ->
        apiRequest.get(apiRoutes.api_series_seasons_path(series_id, {watched: w, format: "json"} ))

      getAll : (r) ->
        apiRequest.get(apiRoutes.api_series_index_path({rating: r, format: "json"}))

      getOwnSeries : (w) ->
        apiRequest.get(apiRoutes.api_watchlists_path({watched: w,  format: "json"}))

      nrOfSeries : ()->
        return series.list.length

      allSeries : ()->
        return series.list

      untickEpisode : (ep_id)->
        apiRequest.delete(apiRoutes.api_user_episode_path(ep_id, {format: "json"}))

      tickEpisode : (ep_id)->
        apiRequest.post(apiRoutes.api_user_episodes_path({format: "json"}),{episode_id: ep_id})

      tickSeason : (season_id)->
        apiRequest.post(apiRoutes.api_user_episodes_path({ whole_season: true, format: "json"}), {id: season_id})

      untickSeason : (season_id)->
        apiRequest.delete(apiRoutes.api_user_episode_path(season_id, {whole_season: true, format: "json"}))
    }
  ]
