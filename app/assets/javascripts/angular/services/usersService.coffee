angular.module "EpiTrackerApp"
  .service "usersProv", ["$http", ($http)->

    return {

      newUser : (user) ->
        user.series = []
        $http.post("api/users/create", user)
        .then(
          ()-> return true
          ,
          ()-> return false)

      checkUser : (user)->

        $http.get('/api/users/'+user.username+'.json')
        .then((response)-> return response.data)
    }
  ]
