angular.module "EpiTrackerApp"
  .service "watchProv", ["$http", "apiRequest", "routeService", ($http, apiRequest, routeService)->
    apiRoutes = routeService.apiRoutes()
    return {
      addEntry : (s_id)->
        apiRequest.post(apiRoutes.api_watchlists_path({format: "json"}), {serie_id: s_id})

      removeEntry :(s_id)->
        apiRequest.delete(apiRoutes.api_watchlist_path({format: "json"}), {serie_id: s_id})
    }
  ]
