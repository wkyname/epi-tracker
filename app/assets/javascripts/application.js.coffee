# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# compiled file.
#
# Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
# about supported directives.
# require bootstrap-material-design
#
#= require js-routes
#= require jquery
#= require jquery_ujs
#= require bootstrap-sprockets
#= require bootstrap-material-design/scripts/material
#= require bootstrap-material-design/scripts/ripples
#= require angular
#= require angular-rails-templates
#= require angular-animate
#= require angular-route
#= require angular-cookie
#= require ng-token-auth
#= require Chart
#= require d3
#= require ./angular/app
#= reuqire_tree .
#= require_self
