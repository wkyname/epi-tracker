class Api::EpisodesController < ApplicationController
  # before_action :authenticate_user!
  def self.seen
    self.user_episodes.where("user_id = 1")
  end

  def index
    @seenEpisodes = {}
    @season = Season.find(params[:season_id])
    # binding.pry
    if @season.nil?
      render json: {errors: "couldn't found!"}, status: :not_found
      return
    end
    if (user_signed_in?)
      authenticate_user!
      @seenEpisodes = @current_user.episodes.where("season_id = ?", params[:season_id])
    end
    @episodesAll = @season.episodes
  end
end
