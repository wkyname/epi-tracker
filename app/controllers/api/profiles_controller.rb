class Api::ProfilesController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def show
    @series = @current_user.series
  end
end
