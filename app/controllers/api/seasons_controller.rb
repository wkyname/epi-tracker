class Api::SeasonsController < ApplicationController
  # before_action :authenticate_user!
  respond_to :json
  def show
    @seenEpisodes = {}
    @season = Season.find(params[:id])
    @episodesAll = @season.episodes
    if (user_signed_in?)
      authenticate_user!
      @seenEpisodes = @current_user.episodes
    end
  end
  def index
    @db_seasons = Serie.find(params[:series_id]).seasons
    if @db_seasons.nil?
      render json: {errors: "couldn't find seasons for serie"}, status: :not_found
    end
    @seasons = []
    if( params[:watched] == "true" && user_signed_in?) 
      authenticate_user!
      @db_seasons.each do |s|
        @seen_episodes = s.episodes & current_user.episodes
        if (@seen_episodes.count < s.episodes.where("first_aired <= ? ", DateTime.now).count)
          @seasons.push(s)
        end
      end
    else
      @seasons = @db_seasons
    end
  end
end
