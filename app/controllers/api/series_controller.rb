class Api::SeriesController < ApplicationController
  before_action :authenticate_user!, only: [:own]
  respond_to :json
  def index
    @userSeries = {}
    if params[:rating].nil?
      @series = Serie.all
    else
      @series = Serie.where("rating >= ?", params[:rating])
    end
    if (user_signed_in?)
      authenticate_user!
      @userSeries = @current_user.series
    end
  end

  def show
    @serie = Serie.find(params[:id])
    @users = @serie.users
    if !@serie.nil?
      if (user_signed_in?)
        render json: { serie: @serie, followers: @serie.users.count, following: (@users.include? @current_user)  }
      else 
        render json: { serie: @serie, followers: @serie.users.count, following: false  }
      end
    else
      render json: { errors: ["couldn't found serie"]}, status: :not_found
    end
  end

  def own
    @series = @current_user.series
  end
end
