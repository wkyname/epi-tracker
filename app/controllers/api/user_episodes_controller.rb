class Api::UserEpisodesController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def create
    # if the whole season is being marked as seen
    if params[:whole_season]
      @season = Season.find(params[:id])
      if @season.nil?
        render json: {errors: "couldn't find seasons for serie"}, status: :not_found
        return
      end
      @serie  = @season.serie
      @season.episodes.where("first_aired <= ?", DateTime.now).each do |episode|
        @entry = UserEpisode.new(user_id: @current_user.id, episode_id: episode.id)
        if @entry.save
          @w = @serie.watchlists.find_by_user_id(@current_user.id)
          if !@w.nil?
            @w.update(seen_episodes: @w.seen_episodes+1)
          end
        end
      end
      render json:{success:true, message:"successfully checked whole season"}
      return

    # if only one episode is marked as seen
    else
      if(Episode.find(params[:episode_id]).first_aired <= DateTime.now )
        @entry = UserEpisode.new(user_id: @current_user.id, episode_id: params[:episode_id])
        @serie = Episode.find(params[:episode_id]).serie
        if @entry.save
          @w = @serie.watchlists.find_by_user_id(@current_user.id)
          if !@w.nil?
            @w.update(seen_episodes: @w.seen_episodes+1)
          end
          render json:{success: true, message:"successfully checked episode"}
          return
        end
        render json:{success: false, message:"didn't check episode"}
        return
      else
        render json:{success: false, message:"episode did not appear yet"}
        return
      end
      render json: {errors: "couldn't check episode"}, status: internal_server_error
      return
    end
  end

  def index
    @episodesAll = Season.find(params[:season_id]).episodes
    @seenEpisodes = @current_user.episodes.where("season_id = ?", params[:season_id])
  end

  def destroy
    @entry = @current_user.user_episodes.find_by_episode_id(params[:id])
    if @entry.destroy
      @w = Episode.find(params[:id]).serie.watchlists.find_by_user_id(@current_user.id)
      if !@w.nil?
        @w.update(seen_episodes: @w.seen_episodes-1)
      end
      render json:{success:true, message: "successfully unchecked episode"}
      return
    end
    render json:{success:false, message: "didn't uncheck episode"}
    return
  end
end
