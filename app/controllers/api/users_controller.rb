class Api::UsersController < ApplicationController
  respond_to :json
  def index
    @users = User.all
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to :action => 'index'
    else
      return false
    end
  end

  def check
    @user = User.where("username = ?", params[:username])
  end

  def show
    @user = User.find_by_nickname(params[:nickname])
    if !@user.nil?
      render json: @user
    else
      render json: { errors: ["can't be null"]}, status: :not_found
    end
  end

  def user_params
    params.require(:user).permit(:name, :username, :email, :password, :shows)
  end

  def seenEpisode
    # binding.pry
    @user = User.where("id = 1").first
    epId = params[:_json]
    if !(@user.seenEpisodes.include? epId)
      @user.seenEpisodes.push epId
    else
      @user.seenEpisodes -=[epId]
    end
    @user.save
  end
end
