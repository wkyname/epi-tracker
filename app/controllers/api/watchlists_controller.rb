class Api::WatchlistsController < ApplicationController
  before_action :authenticate_user!
  respond_to :json
  def index
    # binding.pry
    if params[:watched] == "true"
      @series = @current_user.series
    else
      @watchlists = @current_user.watchlists
      # @series = @current_user.series.includes(:watchlists).where("total_episodes - watchlists.seen_episodes > 0" )
      # binding.pry
    end
  end

  def destroy
    respond_to do |format|
      if (@current_user.series.exists?(id: params[:id]))
        @watch = Watchlist.where("user_id = ? and serie_id = ?", @current_user.id, params[:id]).first
        @watch.destroy
        format.json {render json: {status: "destroyed", followers: Serie.find(params[:id]).users.count, following:false}}
      else
        format.json { render :nothing=>true}
      end
    end
  end

  def create
    if !(@current_user.series.exists?(id: params[:serie_id]))

      @watch = Watchlist.new(user_id: @current_user.id, serie_id: params[:serie_id], seen_episodes: @current_user.episodes.where("serie_id = ? ", params[:serie_id]).count)
      respond_to do |format|
        if @watch.save
          format.html {redirect_to :back}
          format.json { render json: {followers: Serie.find(params[:serie_id]).users.count, following:true }}
        else
          format.html {redirect_to :back}
          format.json { render json: @watch.errors }
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to :back}
        format.json {render :nothing => true}
      end
    end
  end
end
