class Episode < ActiveRecord::Base

  has_many :user_episodes
  has_many :users, through: :user_episodes, source: :user_episode

  belongs_to :serie
  belongs_to :season


  scope :seen, -> (user_id) { where("user_episodes.user_id = ?", user_id) }
end
