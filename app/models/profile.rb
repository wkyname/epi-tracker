class Profile < ActiveRecord::Base
  has_many :series
  belongs_to :user
end