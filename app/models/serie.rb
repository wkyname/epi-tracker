class Serie < ActiveRecord::Base
  has_many :seasons
  has_many :episodes
  has_many :watchlists
  has_many :users, through: :watchlists
  belongs_to :profile

  mount_uploader :image, SeriesImageUploader
  scope :index, -> (i) { where("id = ?", i) } 

  def nextEpisodeDate
    ep = episodes.where("first_aired >= ?", Date.today).first
    if !ep.nil?
      ep.first_aired.strftime('%d %b')
    else
      "TBD"
    end
  end

end
