class User < ActiveRecord::Base
  # Include default devise modules.
  include DeviseTokenAuth::Concerns::User
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :omniauthable

  mount_uploader :image, AvatarUploader
  has_many :user_episodes
  has_many :episodes, through: :user_episodes
  has_many :watchlists
  has_many :series, through: :watchlists, source: :serie

  validates :email, presence: true, uniqueness: true
  validates :nickname, presence: true, uniqueness: true
  validates :password, confirmation: true

  before_save -> do
    skip_confirmation!
  end
end
