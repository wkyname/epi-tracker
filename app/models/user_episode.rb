class UserEpisode < ActiveRecord::Base
  belongs_to :user
  belongs_to :episode

  validates :user_id, presence: true, uniqueness:{scope: :episode_id}
  validates :episode_id, presence: true
end
