class Watchlist < ActiveRecord::Base
  belongs_to :user
  belongs_to :serie

  validates :serie_id, presence: true
  validates :user_id, presence: true
end
