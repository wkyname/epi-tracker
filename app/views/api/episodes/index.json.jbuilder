json.episodes @episodesAll do |episode|
  json.id               episode.id
  json.episode_number   episode.episode_number
  json.season_number    episode.season_number
  json.name             episode.name
  json.overview         episode.overview
  json.first_aired      episode.first_aired
  json.watchable        episode.first_aired < DateTime.now
  json.seen             (@seenEpisodes.include? episode)
end
