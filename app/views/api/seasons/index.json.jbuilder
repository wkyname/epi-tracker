json.seasons @seasons do |season|
  if season.season_number != 0
    json.id             season.id
    json.season_number  season.season_number
    json.serie_id       season.serie_id
    json.has_episodes   true
  end
end
