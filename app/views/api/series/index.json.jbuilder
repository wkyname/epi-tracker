json.series @series do |serie|
  json.id         serie.id
  json.name       serie.name
  json.rating     serie.rating
  json.followers  serie.users.count
  json.following   @userSeries.include? serie
  json.image      do
    json.url      serie.image.thumb.url
  end
end
