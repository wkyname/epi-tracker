json.data @series do |serie|
  # binding.pry
  json.newDate    serie.nextEpisodeDate
  json.followers  serie.users.length
  json.id         serie.id
  json.name       serie.name
  json.rating     serie.rating
  json.image      serie.image
end
