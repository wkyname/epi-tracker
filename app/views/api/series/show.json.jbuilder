json.serie.id         @show.id
json.serie.followers  @show.users.count
json.serie.name       @show.name
json.serie.overview   @show.overview
json.serie.rating     @show.rating
json.image            do
  json.url            @show.image_url
end
