
json.episodes @episodesAll do |episode|
  if !(@seenEpisodes.include? episode) && episode.first_aired < DateTime.now
    json.id               episode.id
    json.episode_number   episode.episode_number
    json.season_number    episode.season_number
    json.name             episode.name
    json.overview         episode.overview
    json.first_aired      episode.first_aired
    json.watchable        true
    json.seen             false
  end
end
