if params[:watched] == "true"
  json.series @series do |serie|
    json.newDate    serie.nextEpisodeDate
    json.followers  serie.users.length
    json.id         serie.id
    json.name       serie.name
    json.rating     serie.rating
    json.following  true
    json.genre      serie.genre
    json.image do
      json.url serie.image_url
    end
  end
else
  json.series @watchlists do |watch|
    @serie = watch.serie
    unseen_episodes = @serie.episodes.where("first_aired <= ? AND  season_number > 0", DateTime.now).count - watch.seen_episodes
    if unseen_episodes > 0
      json.followers  @serie.users.length
      json.id         @serie.id
      json.name       @serie.name
      json.rating     @serie.rating
      json.following  true
      json.genre      @serie.genre
      json.unseen_episodes unseen_episodes
      json.image do
        json.url      @serie.image_url
      end
    end
  end
end
