threads_count = Integer(ENV['MAX_THREADS'] || 4)
threads threads_count, threads_count
workers Integer(ENV['WEB_CONCURRENCY'] || 2)

preload_app!

rackup      DefaultRackup
rails_env = ENV['RAILS_ENV'] || "production"
port        ENV['PORT']     || 3000
environment  rails_env

on_worker_boot do
  require "active_record"
  ActiveRecord::Base.connection.disconnect! rescue ActiveRecord::ConnectionNotEstablished
  ActiveRecord::Base.establish_connection()
end

