
Rails.application.routes.draw do

  get 'welcome/index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view

  mount_devise_token_auth_for 'User', at: 'api/auth'
  namespace :api do
    # resources :series, only: [:index, :show]
    get   'users/:nickname' => 'users#show', as: "users"

    # resources :series, only: [:index, :show] do
    #   resources :seasons, only: [:index, :show] do
    #     resources :episodes, only: [:index, :show]
    #   end
    # end
    resources :series,    only: [:index, :show] do
      resources :seasons, only: [:index, :show]
    end

    resources :seasons,    only: [:index, :show] do
      resources :episodes, only: [:index, :show]
      resources :user_episodes, only: [:index]
    end




    resources :user_episodes, only: [:create, :destroy]
    resources :watchlists,    only: [:create, :index, :destroy]

    # resource :profile, only: [:show] do
    #   resources :series, only: [:index, :show]
    # end


    # get 'seasons/' => 'seasons#index'
    # get 'seasons/:id' => 'seasons#show', as: :season
    # get 'series/own' => 'series#own', as: 'own_series'
    # resources :seasons, only: [:index, :show]

    # get 'episodes' => 'episodes#index', as: :episodes
    # do own
    # get   'series/'               => 'series#index',      via: [:get]
    # get   'series/show/:id'       => 'series#show',       via: [:get]

    # get   'seasons/:id'             => 'seasons#index',     via: [:get]
    # get   'seasons/show/:id/'       => 'seasons#show',      via: [:get]
    # get   'seasons/show/:id?:seen'  => 'seasons#show',      via: [:get]

    # get   'users/'        => 'users#index',       via: [:get]
    # post  'users/create'    => 'users#create',  via: [:post]
    # get   'users/:username' => 'users#check',   via: [:get]
    # post  'users/seenEpisode' => "users#seenEpisode", via: [:post]

    # post  'watchlists/add'    => 'watchlists#add',    via: [:post]

    # post  'user_episodes/new'     =>  'user_episodes#new',      via: [:post]
    # post  'user_episodes/destroy' =>  'user_episodes#destroy',  via: [:post]
    # match '(*path)' => 'welcome#api', via: [:get]
  end
  match '(*path)' => 'welcome#index', via: [:get]
  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
