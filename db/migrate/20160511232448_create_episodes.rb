class CreateEpisodes < ActiveRecord::Migration
  def up
    create_table :episodes do |t|
      t.integer :season_id
      t.integer :season_number
      t.integer :serie_id
      t.integer :episode_number
      t.string :api_episode_id
      t.string :name
      t.string :overview
      t.date :first_aired

      t.timestamps null: false
    end
  end
  def down
    drop_table :episodes
  end
end
