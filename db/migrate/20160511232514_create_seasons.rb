class CreateSeasons < ActiveRecord::Migration
  def up
    create_table :seasons do |t|
      t.string :serie_id
      t.string :api_season_id
      t.integer :season_number

      t.timestamps null: false
    end
  end
  def down
    drop_table :seasons
  end
end
