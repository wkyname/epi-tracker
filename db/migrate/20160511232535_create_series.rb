class CreateSeries < ActiveRecord::Migration
  def up
    create_table :series do |t|
      t.string :api_serie_id
      t.string :name
      t.string :overview
      t.string :imdb_id
      t.float :rating

      t.timestamps null: false
    end
  end
  def down
    drop_table :series
  end
end
