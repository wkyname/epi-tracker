class CreateWatchlists < ActiveRecord::Migration
  def up
    create_table :watchlists do |t|
      t.integer :serie_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
  def down
    drop_table :watchlists
  end
end
