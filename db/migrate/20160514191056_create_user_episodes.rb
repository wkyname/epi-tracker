class CreateUserEpisodes < ActiveRecord::Migration
  def up

    create_table :user_episodes, :primary_key => :ue_id do |t|
      t.integer :user_id
      t.integer :episode_id
    end
    change_column :user_episodes, :ue_id, :integer, :limit => 8
  end
  def down
    drop_table :user_episodes
  end
end


# create_table :user_episodes, :id => false do |t|
#   t.integer :id, :limit => 8, primary_key:true, unique:true, index:true, auto_increment:true, unsigned:true
#   t.integer :user_id
#   t.integer :episode_id
# end
