class AddImageToSeries < ActiveRecord::Migration
  def up
    add_column :series, :image, :string
  end

  def down
    remove_column :series, :image
  end
end
