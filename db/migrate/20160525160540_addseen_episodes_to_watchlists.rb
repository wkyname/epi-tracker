class AddseenEpisodesToWatchlists < ActiveRecord::Migration
  def up
    add_column :watchlists, :seen_episodes, :integer
  end

  def down
    remove_column :watchlists, :seen_episodes
  end
end
