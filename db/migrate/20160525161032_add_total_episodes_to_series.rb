class AddTotalEpisodesToSeries < ActiveRecord::Migration
  def up
    add_column :series, :total_episodes, :integer
  end

  def down
    remove_column :series, :total_episodes
  end
end
