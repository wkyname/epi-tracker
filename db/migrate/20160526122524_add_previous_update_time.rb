class AddPreviousUpdateTime < ActiveRecord::Migration
  def up
    create_table :update_times do |t|
      t.timestamp :time
    end
  end
  def down
    drop_table :update_times
  end
end
