class AddGenreToSerie < ActiveRecord::Migration
  def up
    add_column :series, :genre, :string, array: true, :default => []
  end
  def down
    remove_column :series, :genre
  end
end
