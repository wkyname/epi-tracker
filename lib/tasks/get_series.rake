require "open-uri"
file_url = "app/series_names.txt"

api_key = "114997FF214B9770"

api_root = 'http://thetvdb.com'
get_series_uri = api_root + '/api/GetSeries.php?seriesname='

serie_root = api_root + "/api/"

update_root = serie_root + 'Updates.php?type='
update_none = update_root + 'none'
update_uri = update_root + 'all&time='
@serie_index_uri = serie_root + api_key + "/series/"

banner_root = api_root+ "/banners/"
namespace :get_series do
  desc 'Get series ids'
  task :get_list_series => :environment do
    document = open(file_url).read
    time = Nokogiri::XML(open(update_none))
    update_time = UpdateTime.new({time: Time.at(time.css("Time").inner_text.to_i)})
    update_time.save
    document.split("\n").each do |s|
      doc = Nokogiri::XML(open("#{get_series_uri}#{URI.encode(s)}"))
      doc.css("Series").each do |d|
        result_obj = {
          api_serie_id: d.css('seriesid').inner_text || d.css('id').inner_text,
          name: d.css('SeriesName').inner_text,
          overview: d.css('Overview').inner_text,
          imdb_id: d.css('IMDB_ID').inner_text
        }
        s_n = Serie.find_by_api_serie_id(result_obj[:api_serie_id]) || Serie.new
        bannerXML = Nokogiri::XML(open("#{@serie_index_uri}#{result_obj[:api_serie_id]}/banners.xml"))
        banner_child = bannerXML.css("BannerPath").first
        unless ( banner_child.nil? )
          result_obj[:remote_image_url] = "#{banner_root}#{banner_child.inner_text}"
        end

        serieXML = Nokogiri::XML(open("#{@serie_index_uri}#{result_obj[:api_serie_id]}/all"))
        result_obj[:rating] = serieXML.css("Rating").inner_text
        epiCount = 0
        serieXML.css("Episode").each do |ep|
          binding.pr
          if ( ep.css("SeasonNumber").inner_text != "0" )
            epiCount = epiCount + 1
          end
        end
        # puts
        result_obj[:genre] = serieXML.css("Genre").inner_text.split("|").drop(1)
        result_obj[:total_episodes] = epiCount

        if s_n.update_attributes result_obj
          puts "Serie #{s_n.name} imported succesfully"
        else
          throw "Some error occured"
        end
      end
    end
  end
  # task :import_names => :environment do
  #   document = open(file_url).read
  #   document.split("\n").each do |s|
  #     if ( Serie.find_by(name: s).nil? )
  #       puts "Creating Serie: #{s}"
  #       ret_obj = { name: s }
  #       se = Serie.new(ret_obj)
  #       if se.save
  #         puts "Serie #{s} imported succesfully"
  #       else
  #         throw "Something went wrong"
  #       end
  #     end
  #   end
  # end
  # task :get_ids => :environment do
  #   series = Serie.all
  #   series.each do |s|
  #     if ( s.api_serie_id.nil? )
  #       document = Nokogiri::XML(open("#{series_uri}#{URI.encode(s.name)}"))
  #       document.css("Series").each do |d|
  #         result_obj = {
  #           api_serie_id: d.css('seriesid').inner_text || d.css('id').inner_text,
  #           name: d.css('SeriesName').inner_text,
  #           overview: d.css('Overview').inner_text,
  #           imdb_id: d.css('IMDB_ID').inner_text,
  #           rating: d.css('Rating').inner_text
  #         }
  #         if (d.css("SeriesName").inner_text.eql? s.name)
  #           if s.update_attributes(result_obj)
  #             puts "Serie #{s.name} imported succesfully"
  #           else
  #             throw "Error occured"
  #           end
  #         else
  #           s_n = Serie.new(result_obj)
  #           if s_n.save
  #             puts "Serie #{s_n.name} imported succesfully"
  #           else
  #             throw "Error occured"
  #           end
  #         end
  #       end
  #     end
  #   end
  # end
  task :get_episodes => :environment do
    series = Serie.all
    series.each do |s|
      get_serie_episodes(s)
    end
  end
  task :check_update => :environment do
    last_time = UpdateTime.last
    # binding.pry
    puts "Last update: #{last_time.time}"
    doc = Nokogiri::XML(open("#{update_uri}#{last_time.time.to_i}"))
    puts "Server time: #{Time.at(doc.css('Time').inner_text.to_i)}"
    doc.css("Series").each do |s|
      serie = Serie.find_by_api_serie_id(s.inner_text.to_i)
      if ( serie == nil )
        next
      end
      puts "#{serie.name} needs update [id: #{serie.api_serie_id}]"
    end
  end

  task :get_serie_banners => :environment do
    series = Serie.all
    series.each do |s|
      bannerXML = Nokogiri::XML(open("#{@serie_index_uri}#{s[:api_serie_id]}/banners.xml"))
      banner_child = bannerXML.css("BannerPath").first
      unless ( banner_child.nil? )
        if s.update_attributes({remote_image_url: "#{banner_root}#{banner_child.inner_text}"})
          puts "Updated banner for #{s.name}"
        else
          puts "Error.Abort.Lame"
        end
      end
    end
  end
  task :update => :environment do
    last_time = UpdateTime.last
    doc = Nokogiri::XML(open("#{update_uri}#{last_time.time.to_i}"))
    doc.css("Series").each do |s|
      serie = Serie.find_by_api_serie_id(s.inner_text.to_i)
      if ( serie == nil )
        next
      end
      get_serie_episodes(serie)
      puts "Updated #{serie.name}"
    end
    if last_time.update_attributes(time: Time.at(doc.css("Time").inner_text.to_i))
      puts "Succesfully updated"
    else
      puts "Something went wrong"
    end
  end
end
def get_serie_episodes(s)
  document = Nokogiri::XML(open("#{@serie_index_uri}#{s.api_serie_id}/all"))
  epiCount = 0
  document.css("Episode").each do |e|
    unless ( Season.where(serie_id: s.id).map(&:api_season_id).include?(e.css("seasonid").inner_text) )
      se = Season.new({
        api_season_id: e.css("seasonid").inner_text,
        serie_id: s.id,
        season_number: e.css("SeasonNumber").inner_text
        })
      if se.save
        puts "#{s.name}: Generated season #{se.season_number}"
      else
        throw "An error occured"
      end
    end
    if ( e.css("SeasonNumber").inner_text != "0")
      epiCount = epiCount + 1
    end
    epi = Episode.find_by_api_episode_id(e.css("id").inner_text) || Episode.new
    result_obj = {
      season_id: Season.find_by_api_season_id(e.css("seasonid").inner_text).id,
      serie_id: s.id,
      episode_number: e.css("EpisodeNumber").inner_text,
      api_episode_id: e.css("id").inner_text,
      season_number: e.css("SeasonNumber").inner_text,
      name: e.css("EpisodeName").inner_text,
      overview: e.css("Overview").inner_text,
      first_aired: (Date.parse(e.css("FirstAired").inner_text) unless e.css("FirstAired").inner_text.empty?)
    }
    if epi.update_attributes result_obj
      puts "#{s.name}: Imported episode S#{epi.season.season_number}E#{epi.episode_number}"
    else
      throw "An error occured"
    end
  end
  result_obj = {
    rating: document.xpath("//Series/Rating").inner_text.to_f,
    total_episodes: epiCount,
    genre: document.css("Genre").inner_text.split("|").drop(1)
  }
        # puts
  # result_obj[:genre] = serieXML.css("Genre").inner_text.split("|").drop(1)
  # result_obj[:total_episodes] = epiCount
  # puts "#{episode_uri}#{s.api_serie_id}/all"
  s.update_attributes(result_obj)
end
